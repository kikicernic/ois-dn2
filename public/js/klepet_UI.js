function prikaziSmiley(zapis) {
    var zapisOut = replaceAll(zapis, ';)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" alt="Smiley face">');
    zapisOut = replaceAll(zapisOut, ':)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" alt="Smiley face">');
    zapisOut = replaceAll(zapisOut, '(y)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" alt="Smiley face">');
    zapisOut = replaceAll(zapisOut, ':*', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" alt="Smiley face">');
    zapisOut = replaceAll(zapisOut, ':(', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" alt="Smiley face">');

    return zapisOut;
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

// zamenja vse znak za nek posamezen smiley s likami - lahko bi klical "replace" v zanki
function replaceAll(string, find, replace) {
  return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  // ce je prvi znak vnesenag sporocila "/", gre za ukaz, sicer navadno besedilo
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    // sfiltrirano besedilo
    var filtriranoSporocilo = filtrirajVulgarneBesede(sporocilo);
    
    klepetApp.posljiSporocilo($('#kanal').text(), filtriranoSporocilo);
    
    // nad vnesenim besedilo poklicemo metodo za zamenjavo besed v slike
    var zapisOut = prikaziSmiley(filtriranoSporocilo)
    // pripnemo nov zapis, v katerem znak za smiley-e zamenjali s slikami
    var novElement = $('<div style="font-weight: bold"></div>').append(zapisOut);
    $('#sporocila').append(novElement);
    
    
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace) {
  return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

// tabela psovk
var lines = [];

// vrne besedilo z filtriranimi vulgarnimi besedami
function filtrirajVulgarneBesede(besedilo) {
  var filtriranoBesedilo = besedilo;
  for (var i = 0; i < lines.length; i++) {
      if (filtriranoBesedilo.lastIndexOf(lines[i]) + lines[i].length == filtriranoBesedilo.length) {
         filtriranoBesedilo = filtriranoBesedilo.replace(new RegExp(lines[i] + '$'), Array(lines[i].length+1).join("*"));
      }
      filtriranoBesedilo = replaceAll(filtriranoBesedilo, lines[i] + ' ', Array(lines[i].length+1).join("*") + ' ');
    }
    
    return filtriranoBesedilo;
}

var socket = io.connect();

// globalni spremenljivki za hranjenje trenutnega kanala in vzdeveka
var kanal;
var vzdevek;

$(document).ready(function() {
  // prebere vse psovke v tabelo
  $.get('swearWords.txt', function(data){
    lines = data.split('\n');
       console.log(lines);
  });
      
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      // ob spremembi vzdevka, posodobi globalni vzdevek in posodobi zapis na vrhu pogovora
      vzdevek = rezultat.vzdevek
      $('#kanal').text(vzdevek + " @ " + kanal);
      
      sporocilo = 'Prijavljen si kot ' + vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    // ob spremembi kanala, posodobi globalni kanal in posodobi zapis na vrhu pogovora
    kanal = rezultat.kanal;
    $('#kanal').text(vzdevek + " @ " + kanal);
      
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(prikaziSmiley(sporocilo.besedilo));
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});